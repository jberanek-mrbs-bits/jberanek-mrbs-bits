<html>
<head><title>Sending reminders</title></head>
<body>
<?php

require_once "defaultincludes.inc";

$dst_change = is_dst($month,$day,$year);
$now=mktime(date("H"), date("i"),date("s"),
            $month,$day,$year,is_dst($month,$day,$year,date("H")));
$t_plus_5min = $now + (5 * 60);

$sql = "SELECT R.room_name AS room_name, start_time, end_time, name,
               type,
               E.description AS entry_description, status,
               E.create_by AS entry_create_by
          FROM $tbl_entry E, $tbl_room R
         WHERE E.room_id = R.id
           AND R.area_id = $area
           AND R.disabled = 0
           AND start_time < $t_plus_5min AND start_time > $now
      ORDER BY start_time";   // necessary so that multiple bookings appear in the right order

$res = sql_query($sql);
if (! $res)
{
  fatal_error(0, sql_error());
}

print "<h1>Sending reminders</h1>
<table border=1>
<tr><th>Room</th><th>Start time</th><th>Name</th><th>Descriptiom</th><th>Created by</th></tr>\n";
for ($i = 0; ($row = sql_row_keyed($res, $i)); $i++)
{
  // Each row we've got here is an appointment.
  //  row['room_name'] = Room name
  //  row['start_time'] = start time
  //  row['end_time'] = end time
  //  row['name'] = short description
  //  row['type'] = type (internal/external)
  //  row['entry_description'] = description
  //  row['entry_create_by'] = Creator/owner of entry
  
  print "<tr><td>".$row['room_name']."<td>".time_date_string($row['start_time'])."</td><td>".$row['name']."</td><td>".$row['entry_description']."</td><td>".$row['entry_create_by']."</td></tr>\n";

}
?>
</table>
</body>
</html>
